const imposeBlock = function() {
    console.info("Running Phorsanity");
    const blockUsers = [];
    const allUsers = Object.keys(localStorage);
    allUsers.forEach( function(key) {
        if (localStorage.getItem(key)) {
            console.info(`Blocking user ${key}`);
            blockUsers.push(key);
        }
    });
    var postArray = document.querySelectorAll("li.b-post");
    var quoteArray = document.querySelectorAll("div.bbcode_quote");
    
    for (var post=0; post<postArray.length; post++) {
        if (blockUsers.includes(postArray[post].childNodes[5].childNodes[1].childNodes[1].lastElementChild.childNodes[1].childNodes[1].innerText)) {
            postArray[post].style.display = "none";
        }
    }
    
    for (var quote=0; quote<quoteArray.length; quote++) {
        let quoteBlock  = quoteArray[quote].firstElementChild;
        let posterInfo  = quoteBlock.children[1].firstElementChild;
        let message     = quoteBlock.children[2];
        if (blockUsers.includes(posterInfo.innerText)) {
            posterInfo.previousSibling.textContent = "Content modified by ";
            posterInfo.innerHTML = "Phorsanity";
            posterInfo.nextElementSibling.remove();
            message.innerHTML = "Preserving your sanity, one user at a time.";
        }
    }
}

const targetTree = document.querySelector('div.conversation-content');

const observerConfig = {
    attributes: true,
    childList: true,
    subtree: true
};

const observer = new MutationObserver(function (mutations) {
    if (mutations.length != 0) {
        imposeBlock();
    }
})

observer.observe(targetTree, observerConfig);
imposeBlock();
