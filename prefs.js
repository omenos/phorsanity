const addUser = function() {
    const user = document.getElementById("inputUserName").value;
    if (user == "") {
        console.log("Please provide a username!");
        return;
    }

    const exists = window.localStorage.getItem(user);
    if (exists === null) {
        window.localStorage.setItem(user, true);
        console.log("Adding " + user + " to Phorsanity db.");
    } else {
        console.log("Warning: " + user + " already exists in Phorsanity db!");
    }

    refreshTable();
}

const delUser = function(user) {
    try {
        localStorage.removeItem(user);
        console.log("Removing " + user + " from Phorsanity db.");
    } catch (error) {
        console.error("Error: " + user + " does not exist in Phorsanity db!");
    }
}

const enableUser = function(user) {
    try {
        localStorage.setItem(user, true);
    } catch (error) {
        console.error("Error: " + user + " does not exist in Phorsanity db!");
    }
}

const disableUser = function(user) {
    try {
        localStorage.setItem(user, false);
    } catch (error) {
        console.error("Error: " + user + " does not exist in Phorsanity db!");
    }
}

const refreshTable = function() {
    const userTable = document.getElementById("user-table");
    let generated = "<tr><th>Username</th><th>Status</th><th>Action</th></tr>";

    let keys = Object.keys(localStorage);
    keys.sort();

    keys.forEach(generateTable);
    function generateTable(key) {
        let status = ""
        let action = `<button id=${key}>Delete</button>`
        if (localStorage.getItem(key) == 'true') {
            status = `<input type=\"checkbox\" name=${key} checked/>`
        } else {
            status = `<input type=\"checkbox\" name=${key}/>`
        }
        generated += `<tr><td>${key}</td><td>${status}</td><td>${action}</td></tr>`
    }

    userTable.innerHTML = generated;
}

document.getElementById("add-user").addEventListener("click", addUser);
refreshTable();